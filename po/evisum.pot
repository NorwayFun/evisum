# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the evisum package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: evisum\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-02 13:16+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/bin/system/process.c:76 src/bin/ui/ui_process_list.c:927
msgid "dsleep"
msgstr ""

#: src/bin/system/process.c:77 src/bin/system/process.c:84
#: src/bin/ui/ui_process_list.c:921
msgid "idle"
msgstr ""

#: src/bin/system/process.c:78 src/bin/system/process.c:85
#: src/bin/system/process.c:96 src/bin/ui/ui_process_list.c:915
msgid "running"
msgstr ""

#: src/bin/system/process.c:79 src/bin/system/process.c:86
#: src/bin/ui/ui_process_list.c:917
msgid "sleeping"
msgstr ""

#: src/bin/system/process.c:80 src/bin/system/process.c:87
#: src/bin/ui/ui_process_list.c:919
msgid "stopped"
msgstr ""

#: src/bin/system/process.c:81 src/bin/ui/ui_process_list.c:925
msgid "dead"
msgstr ""

#: src/bin/system/process.c:82 src/bin/system/process.c:92
#: src/bin/system/process.c:95 src/bin/ui/ui_process_list.c:923
msgid "zombie"
msgstr ""

#: src/bin/system/process.c:90
msgid "wait"
msgstr ""

#: src/bin/system/process.c:91
msgid "lock"
msgstr ""

#: src/bin/ui/evisum_ui.c:308 src/bin/ui/evisum_ui.c:541
#, c-format
msgid "%1.0f secs"
msgstr ""

#: src/bin/ui/evisum_ui.c:310
#, c-format
msgid "%1.0f sec"
msgstr ""

#: src/bin/ui/evisum_ui.c:420 src/bin/ui/ui_process_list.c:1384
msgid "Actions"
msgstr ""

#: src/bin/ui/evisum_ui.c:437
msgid "Processes"
msgstr ""

#: src/bin/ui/evisum_ui.c:444
msgid "CPU"
msgstr ""

#: src/bin/ui/evisum_ui.c:451
msgid "Memory"
msgstr ""

#: src/bin/ui/evisum_ui.c:458 src/bin/ui/ui_disk.c:566
msgid "Storage"
msgstr ""

#: src/bin/ui/evisum_ui.c:465 src/bin/ui/ui_sensors.c:270
msgid "Sensors"
msgstr ""

#: src/bin/ui/evisum_ui.c:472 src/bin/ui/ui_network.c:351
msgid "Network"
msgstr ""

#: src/bin/ui/evisum_ui.c:486
msgid "Effects"
msgstr ""

#: src/bin/ui/evisum_ui.c:500 src/bin/ui/ui_util.c:421
msgid "About"
msgstr ""

#: src/bin/ui/evisum_ui.c:525 src/bin/ui/ui_cpu.c:728
msgid "Options"
msgstr ""

#: src/bin/ui/evisum_ui.c:544
msgid "Poll delay"
msgstr ""

#: src/bin/ui/evisum_ui.c:565
msgid "Show kernel threads?"
msgstr ""

#: src/bin/ui/evisum_ui.c:576
msgid "User only?"
msgstr ""

#: src/bin/ui/evisum_ui.c:587
msgid "Display"
msgstr ""

#: src/bin/ui/evisum_ui.c:600
msgid "Display scroll bar?"
msgstr ""

#: src/bin/ui/evisum_ui.c:616
msgid "Alpha"
msgstr ""

#: src/bin/ui/evisum_ui.c:628
#, c-format
msgid "%1.0f %%"
msgstr ""

#: src/bin/ui/ui_cpu.c:651
msgid "Legend"
msgstr ""

#: src/bin/ui/ui_cpu.c:741
msgid "Overlay CPU frequency?"
msgstr ""

#: src/bin/ui/ui_cpu.c:750
msgid "Overlay CPU temperatures?"
msgstr ""

#: src/bin/ui/ui_cpu.c:759
msgid "Confused?"
msgstr ""

#: src/bin/ui/ui_cpu.c:805
msgid "CPU Activity"
msgstr ""

#: src/bin/ui/ui_disk.c:588
msgid "device"
msgstr ""

#: src/bin/ui/ui_disk.c:597
msgid "mount"
msgstr ""

#: src/bin/ui/ui_disk.c:606
msgid "type"
msgstr ""

#: src/bin/ui/ui_disk.c:615
msgid "total"
msgstr ""

#: src/bin/ui/ui_disk.c:626
msgid "used"
msgstr ""

#: src/bin/ui/ui_disk.c:637
msgid "free"
msgstr ""

#: src/bin/ui/ui_disk.c:648
msgid "usage"
msgstr ""

#: src/bin/ui/ui_memory.c:226
msgid "Memory Usage"
msgstr ""

#: src/bin/ui/ui_memory.c:253
msgid "Used"
msgstr ""

#: src/bin/ui/ui_memory.c:260
msgid "Cached"
msgstr ""

#: src/bin/ui/ui_memory.c:267
msgid "Buffered"
msgstr ""

#: src/bin/ui/ui_memory.c:274
msgid "Shared"
msgstr ""

#: src/bin/ui/ui_memory.c:281
msgid "Swapped"
msgstr ""

#: src/bin/ui/ui_memory.c:290
msgid "Video"
msgstr ""

#: src/bin/ui/ui_network.c:163
msgid "<hilight>Total In/Out</>"
msgstr ""

#: src/bin/ui/ui_network.c:174
msgid "<hilight>Peak In/Out</>"
msgstr ""

#: src/bin/ui/ui_network.c:185
msgid "<hilight>In/Out</>"
msgstr ""

#: src/bin/ui/ui_process_list.c:121
msgid "Command"
msgstr ""

#: src/bin/ui/ui_process_list.c:123
msgid "User"
msgstr ""

#: src/bin/ui/ui_process_list.c:125
msgid "Process ID"
msgstr ""

#: src/bin/ui/ui_process_list.c:127 src/bin/ui/ui_process_list.c:1355
#: src/bin/ui/ui_process_view.c:1669
msgid "Threads"
msgstr ""

#: src/bin/ui/ui_process_list.c:129
msgid "CPU #"
msgstr ""

#: src/bin/ui/ui_process_list.c:131
msgid "Priority"
msgstr ""

#: src/bin/ui/ui_process_list.c:133
msgid "Nice"
msgstr ""

#: src/bin/ui/ui_process_list.c:135
msgid "Open Files"
msgstr ""

#: src/bin/ui/ui_process_list.c:137
msgid "Memory Size"
msgstr ""

#: src/bin/ui/ui_process_list.c:139
msgid "Memory Virtual"
msgstr ""

#: src/bin/ui/ui_process_list.c:141
msgid "Memory Reserved"
msgstr ""

#: src/bin/ui/ui_process_list.c:143
msgid "Memory Shared"
msgstr ""

#: src/bin/ui/ui_process_list.c:145
msgid "State"
msgstr ""

#: src/bin/ui/ui_process_list.c:147
msgid "Time"
msgstr ""

#: src/bin/ui/ui_process_list.c:149
msgid "CPU Usage"
msgstr ""

#: src/bin/ui/ui_process_list.c:162
msgid "command"
msgstr ""

#: src/bin/ui/ui_process_list.c:164
msgid "user"
msgstr ""

#: src/bin/ui/ui_process_list.c:166
msgid "pid"
msgstr ""

#: src/bin/ui/ui_process_list.c:168
msgid "thr"
msgstr ""

#: src/bin/ui/ui_process_list.c:170
msgid "cpu"
msgstr ""

#: src/bin/ui/ui_process_list.c:172
msgid "pri"
msgstr ""

#: src/bin/ui/ui_process_list.c:174
msgid "nice"
msgstr ""

#: src/bin/ui/ui_process_list.c:176
msgid "files"
msgstr ""

#: src/bin/ui/ui_process_list.c:178
msgid "size"
msgstr ""

#: src/bin/ui/ui_process_list.c:180
msgid "virt"
msgstr ""

#: src/bin/ui/ui_process_list.c:182
msgid "res"
msgstr ""

#: src/bin/ui/ui_process_list.c:184
msgid "shr"
msgstr ""

#: src/bin/ui/ui_process_list.c:186 src/bin/ui/ui_process_view.c:1440
msgid "state"
msgstr ""

#: src/bin/ui/ui_process_list.c:188
msgid "time"
msgstr ""

#: src/bin/ui/ui_process_list.c:190 src/bin/ui/ui_process_view.c:1461
msgid "cpu %"
msgstr ""

#: src/bin/ui/ui_process_list.c:888
#, c-format
msgid "%i processes: "
msgstr ""

#: src/bin/ui/ui_process_list.c:890
#, c-format
msgid "%i running, "
msgstr ""

#: src/bin/ui/ui_process_list.c:892
#, c-format
msgid "%i sleeping, "
msgstr ""

#: src/bin/ui/ui_process_list.c:894
#, c-format
msgid "%i stopped, "
msgstr ""

#: src/bin/ui/ui_process_list.c:896
#, c-format
msgid "%i idle, "
msgstr ""

#: src/bin/ui/ui_process_list.c:898
#, c-format
msgid "%i dead, "
msgstr ""

#: src/bin/ui/ui_process_list.c:900
#, c-format
msgid "%i dsleep, "
msgstr ""

#: src/bin/ui/ui_process_list.c:902
#, c-format
msgid "%i zombie, "
msgstr ""

#: src/bin/ui/ui_process_list.c:1300
msgid "Debug"
msgstr ""

#: src/bin/ui/ui_process_list.c:1351
msgid "General"
msgstr ""

#: src/bin/ui/ui_process_list.c:1353 src/bin/ui/ui_process_view.c:1658
msgid "Children"
msgstr ""

#: src/bin/ui/ui_process_list.c:1357 src/bin/ui/ui_process_view.c:1680
msgid "Manual"
msgstr ""

#: src/bin/ui/ui_process_list.c:1389
msgid "Start"
msgstr ""

#: src/bin/ui/ui_process_list.c:1393
msgid "Stop"
msgstr ""

#: src/bin/ui/ui_process_list.c:1401
msgid "Info"
msgstr ""

#: src/bin/ui/ui_process_list.c:1406
msgid "Cancel"
msgstr ""

#: src/bin/ui/ui_process_list.c:1569
msgid "Menu"
msgstr ""

#: src/bin/ui/ui_process_list.c:1879
msgid "Search"
msgstr ""

#: src/bin/ui/ui_process_list.c:2196
msgid "Process Explorer"
msgstr ""

#: src/bin/ui/ui_process_view.c:680
#, c-format
msgid "<b>CPU: %.0f%%<br>Size: %s<br>Reserved: %s<br>Virtual: %s</>"
msgstr ""

#: src/bin/ui/ui_process_view.c:857
#, c-format
msgid "No documentation found for %s."
msgstr ""

#: src/bin/ui/ui_process_view.c:986
#, c-format
msgid "%s (%d) - Not running"
msgstr ""

#: src/bin/ui/ui_process_view.c:1153
msgid "Command:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1162
msgid "Command line:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1167
msgid "PID:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1172
msgid "Username:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1177
msgid "UID:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1182
msgid "PPID:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1188
msgid "WQ #:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1190
msgid "CPU #:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1196
msgid "Threads:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1201
msgid "Open Files:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1206
msgid " Memory :"
msgstr ""

#: src/bin/ui/ui_process_view.c:1211
msgid " Shared memory:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1216
msgid " Resident memory:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1221
msgid " Virtual memory:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1226
msgid " Start time:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1231
msgid " Run time:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1236
msgid "Nice:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1241
msgid "Priority:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1246
msgid "State:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1251
msgid "CPU %:"
msgstr ""

#: src/bin/ui/ui_process_view.c:1275
msgid "stop"
msgstr ""

#: src/bin/ui/ui_process_view.c:1281
msgid "start"
msgstr ""

#: src/bin/ui/ui_process_view.c:1287
msgid "kill"
msgstr ""

#: src/bin/ui/ui_process_view.c:1422
msgid "id"
msgstr ""

#: src/bin/ui/ui_process_view.c:1431
msgid "name"
msgstr ""

#: src/bin/ui/ui_process_view.c:1449
msgid "cpu id"
msgstr ""

#: src/bin/ui/ui_process_view.c:1647
msgid "Process"
msgstr ""

#: src/bin/ui/ui_process_view.c:1787
msgid "Unknown"
msgstr ""

#: src/bin/ui/ui_sensors.c:292
msgid "Power"
msgstr ""

#: src/bin/ui/ui_sensors.c:347
msgid "Select..."
msgstr ""

#: src/bin/ui/ui_sensors.c:358
msgid "Thermal"
msgstr ""

#: src/bin/ui/ui_util.c:114
msgid "B"
msgstr ""

#: src/bin/ui/ui_util.c:114
msgid "K"
msgstr ""

#: src/bin/ui/ui_util.c:114
msgid "M"
msgstr ""

#: src/bin/ui/ui_util.c:114
msgid "G"
msgstr ""

#: src/bin/ui/ui_util.c:115
msgid "T"
msgstr ""

#: src/bin/ui/ui_util.c:115
msgid "P"
msgstr ""

#: src/bin/ui/ui_util.c:115
msgid "E"
msgstr ""

#: src/bin/ui/ui_util.c:115
msgid "Z"
msgstr ""

#: src/bin/ui/ui_util.c:511
msgid "Close"
msgstr ""
